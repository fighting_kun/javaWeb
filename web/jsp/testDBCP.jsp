<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 使用 JNDI 数据源 查询SQL语句 --%>
<sql:query var="rs" dataSource="jdbc/databaseweb">
    select id, name, sex from tb_employee
</sql:query>
<%-- 遍历 ResultSet 显示数据 --%>
<c:forEach var="row" items="${rs.rows}">
    ${row.id} ,${row.name} ,${row.sex} <br>
</c:forEach>