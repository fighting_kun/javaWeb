package util;

import com.mysql.jdbc.Driver;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;

public class DbManager {
	public static Connection getConnection() throws SQLException{
		//获取默认的数据库连接
		return getConnection("databaseweb","root","PKC15876739231");
	}
	public static Connection getConnection(String dbName,String userName,
			String password) throws SQLException{
		//获取数据库连接
		String url="jdbc:mysql://localhost:3306/"+dbName+
				"?useUnicode=true&characterEncoding=utf-8&useSSL=false";
		DriverManager.registerDriver(new Driver());
		return DriverManager.getConnection(url, userName, password);
		//获取连接
	}
	public static void setParams(PreparedStatement preStmt,Object... params)
	//设置参数
		throws SQLException{
		if(params==null || params.length==0)
			return ;
		for(int i=1; i<=params.length ;i++){
			Object param = params[i-1];
			if(param==null){
				preStmt.setNull(i, Types.NULL);
			}else if(param instanceof Integer ){
				preStmt.setInt(i, (Integer)param);
			}else if(param instanceof String){
				preStmt.setString(i, (String)param);
			}else if(param instanceof Double){
				preStmt.setDouble(i, (Double)param);
			}else if(param instanceof Long){
				preStmt.setLong(i, (Long)param);
			}else if(param instanceof Timestamp){
				preStmt.setTimestamp(i, (Timestamp)param);
			}else if(param instanceof Boolean){
				preStmt.setBoolean(i, (Boolean)param);
			}else if(param instanceof Date){
				preStmt.setDate(i, (Date)param);
			}
		}
	}
	public static int executeUpdate(String sql) throws SQLException{
		//执行SQL语句，返回影响行数
		return executeUpdate(sql,new Object[]{});
	}
	public static int executeUpdate(String sql,Object... params) throws SQLException{
		//执行SQL语句，返回影响行数
		Connection conn=null;
		PreparedStatement preStmt=null;
		try{
			conn=getConnection();
			preStmt=conn.prepareStatement(sql);//预编译带参数的SQL语句
			setParams(preStmt,params);//预置参数
			return preStmt.executeUpdate();
		}finally{
			if(preStmt!=null)
				preStmt.close();
			
			if(conn!=null)
				conn.close();
			
		}
	}
	/**
	 * 获取总数
	 * @param sql 格式必须为 SELECT count(*) FROM ...
	 * @return
	 * @throws SQLException 
	 */
	public static int getCount(String sql) throws SQLException{
		Connection conn=null;
		Statement stmt=null;
		ResultSet rs=null;
		try{
			conn=getConnection();//获得连接
			stmt=conn.createStatement();
			rs=stmt.executeQuery(sql);//执行SQL语句
			return rs.getInt(1);//返回第一列数据（查询到的记录的总数）
		}finally{
			if(rs!=null) rs.close();
			if(stmt!=null) stmt.close();
			if(conn!=null) conn.close();
		}
	}
}
