package DAO;

import bean.Department;
import bean.Employee;
import util.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PKC on 2017/2/18.
 */
public class EmployeeDAO {
    /**
     * 插入一条记录
     */
    public static int insert(Employee employee) throws SQLException {
        String sql = " INSERT INTO tb_employee" +
                "(department_id, name, sex, employed_date) VALUES (?, ?, ?, ?)";
        return DbManager.executeUpdate(sql,employee.getDepartment().getId(),employee.getName(),
                employee.getSex(),employee.getEmployedDate());
    }

    /**
     * 保存一条记录
     */
    public static int save(Employee employee) throws SQLException {
        String sql = "UPDATE tb_employee SET " +
                "department_id = ?, name = ?, sex = ?, employed_date = ?" +
                " WHERE id = ?";
        return DbManager.executeUpdate(sql,employee.getDepartment().getId(),
                employee.getName(), employee.getSex(), employee.getEmployedDate(),
                employee.getId());
    }

    /**
     * 删除 department
     */
    public static int delete(Integer id) throws SQLException {
        String sql = "DELETE FROM tb_employee WHERE id = ?";
        return DbManager.executeUpdate(sql,id);
    }

    /**
     * 查找一条记录
     */
    public static Employee find(Integer id) throws SQLException {
        String sql = "SELECT * FROM tb_employee WHERE id = ?";
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DbManager.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1,id);
            resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setEmployedDate(resultSet.getDate("employed_date"));
                employee.setSex(resultSet.getString("sex"));
                Department department = DepartmentDAO.find(resultSet.getInt("department_id"));
                employee.setDepartment(department);
                return employee;
            }else
                return null;

        }finally{
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
    }

    /**
     * 列出所有员工的资料
     */
    public static List<Employee> listEmployee() throws SQLException {
        String sql = "SELECT * FROM tb_employee ORDER BY id DESC";
        List<Employee> list = new ArrayList<Employee>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DbManager.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Employee employee = new Employee();
                employee.setId(resultSet.getInt("id"));
                employee.setName(resultSet.getString("name"));
                employee.setEmployedDate(resultSet.getDate("employed_date"));
                employee.setSex(resultSet.getString("sex"));
                Department department = DepartmentDAO.find(resultSet.getInt("department_id"));
                employee.setDepartment(department);
                list.add(employee);
            }
        }finally {
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return list;
    }
}
