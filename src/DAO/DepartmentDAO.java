package DAO;

import bean.Department;
import util.DbManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by PKC on 2017/2/18.
 */
public class DepartmentDAO {
    /**
     * 插入一条 department 记录
     */
    public static int insert(Department department) throws SQLException {
        String sql = "INSERT INTO tb_department (name) values (?)";
        return DbManager.executeUpdate(sql,department.getName());
    }

    /**
     * 保存 department
     */
    public static int save(Department department) throws SQLException {
        String sql = "UPDATE tb_department SET name = ? WHERE id = ?";
        return DbManager.executeUpdate(sql,department.getName(),department.getId());
    }

    /**
     * 删除 department
     */
    public static int delete(Integer id) throws SQLException {
        String sql = "DELETE FROM tb_department WHERE id = ?";
        return DbManager.executeUpdate(sql,id);
    }

    /**
     * 查找一条 department 记录
     */
    public static Department find(Integer id) throws SQLException {
        String sql = "SELECT * FROM tb_department WHERE id = ?";
        //SQL语句
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try{
            connection = DbManager.getConnection(); //获取连接
            preparedStatement = connection.prepareStatement(sql); //创建 PrepareStatement
            preparedStatement.setInt(1,id); //设置参数
            resultSet = preparedStatement.executeQuery(); //执行查询
            if(resultSet.next()){
                Department department = new Department();
                department.setId(id);
                department.setName(resultSet.getString("name"));
                return department;
            }else
                return null;
        } finally {
            if(resultSet !=null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
    }

    /**
     *列出所有的 department
     */
    public static List<Department> listDepartment() throws SQLException {
        String sql = "SELECT * FROM tb_departMent ORDER BY id DESC"; //SQL语句，结果按照ID降序排列
        List<Department> list = new ArrayList<>();
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet =  null;
        try{
            connection = DbManager.getConnection();
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();
            while(resultSet.next()){
                Department department = new Department();
                department.setId(resultSet.getInt("id"));
                department.setName(resultSet.getString("name"));
                list.add(department);
            }
        } finally{
            if(resultSet != null) resultSet.close();
            if(preparedStatement != null) preparedStatement.close();
            if(connection != null) connection.close();
        }
        return list;
    }
}
