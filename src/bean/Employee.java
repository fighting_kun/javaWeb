package bean;

import java.sql.Date;

/**
 * Created by PKC on 2017/2/17.
 */
public class Employee {
    private Department department;
    private Integer id;
    private String name;
    private String sex;
    private Date employedDate;

    public Department getDepartment() {
        return department;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSex() {
        return sex;
    }

    public Date getEmployedDate() {
        return employedDate;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public void setEmployedDate(Date employedDate) {
        this.employedDate = employedDate;
    }
}
